<?php

namespace App\Http\Controllers;

use App\Szemely;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function index()
    {
        return ['szemelyek' => Szemely::all()->mapWithKeys(function ($item) {
            return [$item->id => $item];
        })];
    }

    public function store()
    {
        $id = DB::table('szemelyek')->insertGetId([
            'nev' => request('nev'),
            'szuletes' => request('szuletes'),
            'email' => request('email'),
            'felettes' => request('felettes') == -1 ? null : request('felettes')
        ]);

        $szemely = Szemely::find($id);

        if (request('kep') != -1 && request()->files->count() > 0) {
            $szemely->setImage(request()->file('kep'));
        }

        return 'ok';
    }

    public function beosztottak(Szemely $szemely)
    {
        return $szemely->beosztottak();
    }

    public function update(Szemely $szemely)
    {
        $szemely->nev = request('nev');
        $szemely->email = request('email');
        $szemely->szuletes = request('szuletes');
        $szemely->felettes = request('felettes') == -1 ? null : request('felettes');

        if (request('kep') == -1) {
            $szemely->deleteImage();
        }

        if (request('kep') != -1 && request()->files->count() > 0) {
            $szemely->setImage(request()->file('kep'));
        }

        $szemely->save();
        return 'ok';
    }

    public function delete(Szemely $szemely)
    {
        $szemely->deleteImage();
        $szemely->delete();
        return 'ok';
    }
}
