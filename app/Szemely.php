<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class Szemely extends Model
{
    protected $table = 'szemelyek';
    protected $with = ['beosztottak'];

    public function beosztottak()
    {
        return $this->hasMany(Szemely::class, 'felettes');
    }

    public function setImage($image)
    {
        $image_resize = Image::make($image->getRealPath());
        if ($image_resize->getWidth() > 500 || $image_resize->getHeight() > 500) {
            $image_resize->fit(500, 500, null, 'top');
        }
        $image_resize->save(public_path("img") . "/$this->id.png");
        $this->kep = true;
        $this->save();
    }

    public function deleteImage()
    {
        if (file_exists(public_path("img" . "/$this->id.png"))) {
            unlink(public_path("img" . "/$this->id.png"));
        }
        $this->kep = false;
        $this->save();
    }
}
