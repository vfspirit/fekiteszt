<?php

Route::get('/', function () {
    return view('base');
});

Route::get('/api/szemelyek', 'ApiController@index');
Route::post('/api/szemelyek', 'ApiController@store');
Route::get('/api/szemelyek/{szemely}/beosztottak', 'ApiController@beosztottak');
Route::post('/api/szemelyek/{szemely}', 'ApiController@update');
Route::post('/api/szemelyek/{szemely}/delete', 'ApiController@delete');
