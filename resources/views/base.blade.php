<!doctype html>
<html lang="hu">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
        <title>Személynyilvántartó</title>
    </head>
    <body>
        <div id="app"></div>
        <script src="{{ URL::asset('js/app.js') }}"></script>
    </body>
</html>
