<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSzemelyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('szemelyek', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nev');
            $table->string('email');
            $table->date('szuletes');
            $table->boolean('kep')->default(false);
            $table->integer('felettes')->nullable();

            $table->foreign('felettes')->references('id')->on('szemelyek')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('szemelyek');
    }
}
