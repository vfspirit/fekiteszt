<?php

use Illuminate\Database\Seeder;

class SzemelyekTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('szemelyek')->insert([
            'nev' => 'Próba Elemér',
            'email' => 'p.elemer@teszt.hu',
            'szuletes' => date_create('1991-02-13')
        ]);
        DB::table('szemelyek')->insert([
            'nev' => 'Lakatos Pistván',
            'email' => 'elpista@teszt.hu',
            'szuletes' => date_create('1991-02-13')
        ]);
        DB::table('szemelyek')->insert([
            'nev' => 'Szamuráj Béla',
            'email' => 'szomoruszamuraj@teszt.hu',
            'szuletes' => date_create('1991-02-13')
        ]);
        DB::table('szemelyek')->insert([
            'nev' => 'Elméleti Fizikus',
            'email' => 'elfi@teszt.hu',
            'szuletes' => date_create('1991-02-13')
        ]);
    }
}
